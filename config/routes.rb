Rails.application.routes.draw do
  resources :keyword_mappings
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get '/test/eat', to:'test#eat'
  get '/test/request_headers', to: 'test#request_headers'
  get '/test/request_body', to: 'test#request_body'
  get '/test/response_headers', to: 'test#response_headers'
  get '/test/response_body', to: 'test#show_response_body'
  get '/test/sent_request', to:'test#sent_request'
  post '/webhook/testApi',to:'webhook#testApi'
  post '/webhook/sendMessage',to:'webhook#sendMessage'
end
